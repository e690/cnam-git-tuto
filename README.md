# Outils de Productivité

Yoann Bertuol - 27/09/2021
---------------------

## TP1

### Adresse du Repo

https://gitlab.com/e690/cnam-git-tuto.git

### Votre premier dépôt

Pour initialiser notre premier dépôt, on utilise la commande `git init`.

la configuration de nos identifiants se réalise grâce à la commande `git config --global` avec les deux sous sections `user.email` et `user.name`.

L'état du dépôt s'affiche avec la commande `git status`.

On ajoute le fichier `index.html` dans notre commit avec la commande `git add index.html`, un ajout de tout notre dossier est aussi possible avec la commande `git add -a`.

Pour enregistrer notre commit, il faut effectuer la commande `git commit` ce qui nous ouvrira une invite de commande **VIM** dans laquelle nous pourront renseigner le nom de notre commit. Pour éviter cet invite, on peut préciser l'option `-m` puis préciser le nom du commit entre guillemets.

### Questions

1. Deux commits peuvent-ils avoir le même identifiant (hash) ? Si oui, dans quel cas ?

    Ce n'est pas possible car Git utilise toutes les informations mises à sa disposition (date, heure, auteur, contenu...) rende le hash d'un commit unique.

2. Peut-on ajouter en une seule commande tous les fichiers du répertoire de travail à l'index ? Si oui, comment ?

    Oui il est possible d'ajouter toutes les modifications de notre projet en même temps grâce à la commande `git add -a` et `git add *`

3. Peut-on ajouter un fichier à l'index si celui-ci n'est pas dans le répertoire de travail ? Si oui, comment ?

    Non, lors de l'initialisation d'un projet, un dossier `.git` est généré dans le répertoire de travail, il ne peut alors pas considérer un fichier qui se trouve en dehors de ce dossier.

4. Pensez-vous que certaines entreprises n'ont pas besoin d'utiliser de VCS ? Justifiez votre réponse en donnant quelques exemples.

    Non pour moi toutes les entreprises se doivent d'utiliser des VCS pour plusieurs raisons :

    - Un VCS permets une livraison de version de projet, et donc de permettre des mises à jour des logiciels livrés par l'entreprise.
    - Un VCS permets aussi de tester son logiciel en amont de la livraison et donc d'éviter les régressions grâce à l'implémentations de tests automatiques à chaque version.
    - Les logiciels actuels prennent plusieurs mois voire années à être réalisés, et demande des équipe entières de développement. Sans cet outil collaboratif, nous ne pourriont arriver à un niveau de qualité de produits tels que ceux livrés en ce moment.
    - Lors de soucis majeurs, ne pas utiliser un VCS rends le retour en arrière fastidieux.

5. Une fois un fichier ajouté à l'index, est-il possible de l'en retirer ? Si oui, par quel moyen ?

    La commande `git rm` permets de retirer un fichier du dépôt, tout en le gardant localement.

## TP2

### Créer des *feature branches*

Pour créer une branche, j'utilise la commande `git checkout -b ajout-styles` pour y ajouter la feature de style à mon dépôt.

Un commit fut créé avec la commande `git commit -m "feat: adding styles"`

Pour changer de branche, j'utilise la commande `git checkout`.

Pour créer ma seconde branche, j'utilise la commande `git checkout -b modification-textes` pour y ajouter la feature textuelle à mon dépôt.

Un commit fut créé avec la commande `git commit -m "feat: modify paragraph"`

Puis après modification du titre, un commit fut créé avec la commande `git commit -m "feat: adding title"`

### Rapatrier le travail sur la branche `master`

Pour fusionner les branches, j'utilise la commande `git merge modification-textes`.

Pour fusionner la seconde branche, j'utilise la commande `git merge ajout-styles`.

Il y a un conflit qui fut corrigé, puis ajouter à la tentative de fusion grâce à la commande `git add index.html`. Après un commit nommé *Merge adding style*, la fusion est terminée.

Pour supprimer les branches on utilise la commande `git branch -d <branch>`.

### Questions

Une fusion en *fast-forward* est une fusion en avance rapide, récupérant les modifications d'une branche différente permettant d'éviter des commit de merge, et d'intégrer l'historique de la branche distante (et donc éviter les "squash" de commits, influant sur l'intégrité de l'historique du projet).

Il y a eu un conflit sur le second merge car l'historique des deux branches étaient différents sur ce qui devait leurs bases partagées, et donc on a voulu modifier un fichier alors que des modification non connus de la branche courante ont été effectués. Cela arrive surtout lorsque les modifications se trouvent sur les mêmes lignes des fichiers concernées, ici le paragraphe bleu.

Les branches locales peuvent être récupérés si elles ont été enregistrés sur le dépôt distant, considérés comme n'importe quel branche. Si ce n'a pas été le cas, elle peut être récupérer grâce à son encodage SHA, trouvé grâce à la commande `git reflog --no-abbrev` puis `git checkout <sha>`.

Le graphe présent sur Gitlab permets de voir les branches et commits mises en cause sur la branche étudié. Je trouve dommage que la branche `modification-textes` ne soit pas présente dans ce graphe, dû au fast-forward. La branche `ajout-styles` est elle présente du au conflit.
